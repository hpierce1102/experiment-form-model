(function() {
    var _ = self.TextField = function(DOMNodes) {
        this.inputNode = DOMNodes[0];
        this.validationRules = [];
        this.invalidHandler = function() {
            console.log("invalid");
        };
        this.validHandler = function() {
            console.log("valid");
        };

        this.inputNode.addEventListener('change', function() {
            this.runValidation();
        }.bind(this));
    };

    _.prototype = Object.create(new BaseField(), {});

    Object.defineProperty(_.prototype, 'value', {
        get : function() {
            return this.inputNode.value;
        },
        set : function(value) {
            this.inputNode.value = value;
        }
    })
})();