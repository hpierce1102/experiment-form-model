(function() {
    var _ = self.FieldFactory = function() {

    };

    _.prototype = {

    };

    _.createField = function(DOMNodes) {
        var field;

        switch(DOMNodes[0].type) {
            case "text" :
                field = new TextField(DOMNodes);
                break;
            case "radio" :
                field = new RadioField(DOMNodes);
                break;
            case "submit":
                // TODO: make a submit field
                field = new TextField(DOMNodes);
                break;
            default :
                throw new Error('Could not create a field based on ' + DOMNodes[0].type + ' type');
                break;
        }

        return field;
    }
})();

