Experiment - Create a model to add utility to HTML forms
========================================================

Goal:
-----

Model a `<form>` as a JavaScript object to provide a framework for adding custom validation rules and sticky fields 
(submitted data is left in the inputs when you return). The model should be unaware of business logic and minimally coupled
to the structure of the HTML.

Strategy:
---------

A **Form** object would contain a list of various **Field** objects. Each of the Field objects should contain a list of
validation functions, a validated callback, and an invalidated callback. When a field is validated, its validation functions
execute. If any of them fail (returning `false`), the invalidated callback executes. If all of the rules pass, the validated 
callback executes.

The Form itself would have to expose an interface to apply these rules so that the calling context can add them without 
directly mangling the innards of the Form object. This interface would be an arbitrary selector 
(`document.querySelector(selector)`) because of the crazy flexibility it offers.

Pros:
-----
* The named classes for objects is nice.

* The interface for adding rules and callbacks to multiple fields worked very well.

Cons:
-----
* Fields types are _very_ unique in the way they are created in HTML. Modeling them is difficult. Radios for instance,
have many `<input>` elements used to create a single radio field. Which `<input>` are you supposed to use when attempting
to select these elements? All of them? The first of them? Some meta element describing all of them?
I'm not sure there's a great answer.

* This is a monolith. I wouldn't want to maintain this.

Notes:
------

I ended up doing this with ES5 because I wanted to bump into issues with IE 11. I succeeded. Apparently IE 11 doesn't support
`Element.closest()` or various `NodeList.*` functions. While I thought about polyfilling them, I never got around to it.
I probably would have tried achieving this by wrapping `document.querySelectorAll()` in a constructor that provided an object
that made the methods I needed accessible.

None of the features added were optional. You had to take all of them or none of them. I thought about adding more switches
and parameters on the Form constructor to disable these. But all that just makes working with this even more of a nightmare.
The alternative could be making `Form()` a _factory factory_, in that calling `new Form(enableSticky)` returned another factory.
But again. Complexity. I wouldn't want to maintain this. 

The stickiness left something to be desired. The page history isn't taken into account, so _successfully_ submitted a form
would continue to remember the input values. It also got in the way of the stupid submit button. The stickiness would 
remove the value for the submit buttons resulting in a nameless button. That's not great. The solution might be to 
actually create a SubmitField object and disable its value setter, but that feels like the kind of opinionated change
I'd regret down the line _that one time I actually want to mutate the submit value_.

I had plans to experiment more with the history, hence the redHerring.php and target.php files.

I think a better solution to this problem would be to separate validation and stickiness into separate concerns and _not_
attempt to create a named model of a form object. The form object frequently felt like an extra, unnessesary layer between
my HTML and the `Fields` I wanted to work with.

Apparently `sessionStorage` doesn't allow you to store entire objects. I thought that was weird. I attempted to throw the 
entire form object into session storage to achieve the stickiness, but apparently session storage doesn't allow this.

I ran this using `$ php -S localhost:8000`.
