(function() {
    var _ = self.BaseField = function() {
        return {
            addValidationRule : function(callback) {
                if (this.validationRules.indexOf(callback) === -1)
                {
                    this.validationRules.push(callback);
                }
            },
            removeValidationRule : function(callback) {
                if (this.validationRules.indexOf(callback) !== -1) {
                    this.validationRules.splice(this.validationRules.indexOf(callback), 1);
                }
            },
            isValid : function() {
                for (var rule in this.validationRules) {
                    if (!this.validationRules[rule](this)) {
                        return false;
                    }
                }

                return true;
            },
            runValidation : function() {
                if (this.isValid()) {
                    this.validHandler(this);
                } else {
                    this.invalidHandler(this);
                }
            }
        }
    };

    _.prototype = {
        // Since this class is abstract, the functions should be set in the constructor.
    };
})();
