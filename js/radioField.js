(function() {
    var _ = self.RadioField = function(DOMNodes) {
        this.inputNode = DOMNodes[0]; // Imperfection - This is needed to query elements from the form.
        this.inputNodes = DOMNodes;
        this.validationRules = [];
        this.invalidHandler = function() {
            console.log("invalid");
        };
        this.validHandler = function() {
            console.log("valid");
        };

        this.inputNodes.forEach(function(DOMNode) {
            DOMNode.addEventListener('change', function() {
                this.runValidation();
            }.bind(this));
        }.bind(this));
    };

    _.prototype = Object.create(new BaseField(), {});

    Object.defineProperty(_.prototype, 'value', {
        get : function() {
            for (var DOMNode in this.inputNodes) {
                if (this.inputNodes[DOMNode].checked && this.inputNodes.hasOwnProperty(DOMNode)) {
                    return this.inputNodes[DOMNode].value;
                }
            }

            return '';
        },
        set : function(value) {
            for (var DOMNode in this.inputNodes) {
                if (this.inputNodes[DOMNode].value === value.toString() && this.inputNodes.hasOwnProperty(DOMNode)) {
                    this.inputNodes[DOMNode].checked = true;
                }
            }
        }
    });

    function getValue() {

    }
})();