(function() {
    var _ = self.Form = function(DOMNode) {
        this.formNode = DOMNode;
        this.inputs = [];

        // TODO: IE 11 doesn't support various functions on NodeLists
        // https://developer.mozilla.org/en-US/docs/Web/API/NodeList#Browser_compatibility
        var inputs = Array.from(this.formNode.querySelectorAll('input'));

        // Radio buttons have multiple <inputs>, but each <input> combined refers to a single field.
        // We only want to process each field once, so we eliminate duplicate inputs.
        inputs.reduce( function(nodeNames, carry, inputNode) {
                if (nodeNames.indexOf(inputNode.name) === -1) {
                    carry.push(inputNode);
                    nodeNames.push(inputNode.name);
                }

                return carry;
            }.bind(null, []), [])
            .forEach( function(inputNode){
                var inputsOfSameName;
                if (inputNode.name.length > 0) {
                    inputsOfSameName = Array.from(this.formNode.querySelectorAll('input[name="' + inputNode.name + '"]'));
                } else {
                    inputsOfSameName = [ inputNode ];
                }
                this.inputs.push( new FieldFactory.createField(inputsOfSameName) );
            }.bind(this));

        this.inputs.forEach(function(field) {
            var value = sessionStorage.getItem(this.formNode.id + '.' + field.inputNode.name);
            if (value !== null) {
                field.value = value;
            }
        }.bind(this));
        this.runAllValidation();

        this.formNode.addEventListener('submit', function(e) {
            this.inputs.forEach(function(field) {
                sessionStorage.setItem(this.formNode.id + '.' + field.inputNode.name, field.value);
            }.bind(this));
        }.bind(this));
    };

    _.prototype = {
        addValidationRuleToFields: function(selector, validationCallback) {
            var fieldsToModify = this.inputs.filter(function(field) {
                return field.inputNode.matches(selector);
            });

            fieldsToModify.forEach(function(textField) {
                return textField.addValidationRule(validationCallback);
            })
        },
        removeValidationRuleToFields: function(selector, validationCallback) {
            var fieldsToModify = this.inputs.filter(function(field) {
                return field.inputNode.matches(selector);
            });

            fieldsToModify.forEach(function(textField) {
                return textField.removeValidationRule(validationCallback);
            })
        },
        addValidHandler: function(selector, callback) {
            var fieldsToModify = this.inputs.filter(function(field) {
                return field.inputNode.matches(selector);
            });

            fieldsToModify.forEach(function(textField) {
                return textField.validHandler = callback;
            })
        },
        addInvalidHandler: function(selector, callback) {
            var fieldsToModify = this.inputs.filter(function(field) {
                return field.inputNode.matches(selector);
            });

            fieldsToModify.forEach(function(textField) {
                return textField.invalidHandler = callback;
            })
        },
        runAllValidation: function() {
            this.inputs.forEach(function(field) {
                field.runValidation();
            }.bind(this));
        }
    };

    _.createFromSelector = function() {

    };
})();