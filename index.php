<link href="css/styles.css" rel="stylesheet"/>
<script src="js/baseField.js"></script>
<script src="js/textField.js"></script>
<script src="js/radioField.js"></script>
<script src="js/fieldFactory.js"></script>
<script src="js/form.js"></script>
<script>
    var required = function(field) {
        return field.value.length > 0;
    };

    document.addEventListener('DOMContentLoaded', function() {
        var form = new Form(document.querySelector('form'));
        console.log(form);

        form.addValidationRuleToFields('*', required);

        form.addInvalidHandler('*:not([type="submit"])', function(field) {
//        TODO IE 11
            var formGroup = field.inputNode.closest('.form-group');
            formGroup.classList.add('has-error');
        });

        form.addValidHandler('*', function(field) {
            var formGroup = field.inputNode.closest('.form-group');
            formGroup.classList.remove('has-error');
        });

        document.querySelector('input[type="radio"][value="myRadio1"]')
            .addEventListener('change', function(e) {
                form.addValidationRuleToFields('input[name="foo"]', required);
                form.removeValidationRuleToFields('input[type="text"]:not([name="foo"])', required);
                form.runAllValidation();
            });

        document.querySelector('input[type="radio"][value="myRadio2"]')
            .addEventListener('change', function(e) {
                form.addValidationRuleToFields('input[name="bar"]', required);
                form.removeValidationRuleToFields('input[type="text"]:not([name="bar"])', required);
                form.runAllValidation();
            });

        document.querySelector('input[type="radio"][value="myRadio3"]')
            .addEventListener('change', function(e) {
                form.addValidationRuleToFields('input[name="baz"]', required);
                form.removeValidationRuleToFields('input[type="text"]:not([name="baz"])', required);
                form.runAllValidation();
            });
    });
</script>
<form method="post" action="target.php" id="example-form">
    <div class="form-group">
        <label>
            Foo:
            <input type="text" name="foo"/>
        </label>
        <span>This field is invalid</span>
    </div>

    <div class="form-group">
        <label>
            Bar:
            <input type="text" name="bar"/>
        </label>
        <span>This field is invalid</span>
    </div>


    <div class="form-group">
        <label>
            Baz:
            <input type="text" name="baz"/>
        </label>
        <span>This field is invalid</span>
    </div>


    <div class="form-group">
        MyRadio:
        <div>
            <label>
                <input type="radio" name="myRadio" value="myRadio1">
                MyRadio1 - Foo is required
            </label>
        </div>

        <div>
            <label>
                <input type="radio" name="myRadio" value="myRadio2">
                MyRadio2 - Bar is required
            </label>
        </div>

        <div>
            <label>
                <input type="radio" name="myRadio" value="myRadio3">
                MyRadio3 - Baz is required
            </label>
        </div>
        <span>This field is invalid</span>
    </div>
    <input type="submit"/>
</form>